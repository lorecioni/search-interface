<!DOCTYPE html>
<?php 
include('config.php');
?>
<html lang="en">
<head>
	<title>micc Search</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="euTV search interface">
	<meta name="keywords" content="search interface, eutv, music, audio, documents, images, videos">
	<meta name="author" content="Lorenzo Cioni">
	<link rel="image_src" href="img/micc-logo.png" />
	<link rel="icon" href="favicon.ico" type="image/x-icon" />
	<link rel="SHORTCUT ICON" href="favicon.ico" type="image/x-icon" />
	<link  rel="stylesheet" href="css/search-template.css" type="text/css">
	<script type="text/javascript" src="lib/jquery-2.0.3.js"></script>
	<script type="text/javascript" src="lib/jquery.form.js"></script>
	<script type="text/javascript" src="js/config.js"></script>
	<script type="text/javascript" src="js/search.js"></script>
	<script type="text/javascript" src="js/interface.js"></script>
</head>
<body>
  <div id="container">
	<div id="logo">
		<a href="#">
			<img src="img/micc-logo.png" alt="micc logo">
		</a>
	</div>
	<?php 
		session_start();
		if(isset($_SESSION['user'])){
			echo '<div id="user-area-logged">';
			echo '<a href="user-collection">';
			echo '<img class="user-avatar" src="'.$_SESSION['avatar'].'" alt="avatar">'.$_SESSION['user'].'</a>';
		?>
			<form action="index.php" method="post">
				<input type="submit" name="logout" class="logout-button" value="Logout">
			</form>
		<?php
		}
		else {
	?>
	<div id="user-area">
		<a href="login.php">Login</a>
		<a href="register.php" class="tab">Register</a>
	<?php 
	}	
	if(isset($_POST["logout"])){
		session_destroy();
		header ("Location: index.php");
	}
	?>
	</div>
	<?php 
	if(isset($_SESSION['user'])){
	?>
	<div id="upload-wrapper">
		<a href="upload">
			Media upload
			<img alt="upload" src="img/upload-icon.png">
		</a>
	</div>
	<?php 
	}
	?>
	<div id="main-wrapper">	
		<form action="main/index.php" id="form-search" method="get">
			<input type="text" name="q" id="search-form" placeholder="Search" autocomplete="off" autofocus="autofocus"/>
		</form>	
		<form id="upload-form" action="php/upload.php" method="post" enctype="multipart/form-data">
				<div id="filedrag">
    				<div class="dropzone" id="dragandrophandler">
      					<span id="drag-text">Drop file here<br> or</span>					
					</div>
					<div id="progress">
        				<div id="bar"></div>
        				<div id="percent">0%</div>
					</div>
    				<div id="upload-message"></div>
     				<input type="file" size="60" name="file" id="query-file" />
					<input type="button" name="search-file" value="Search" class="search-button"/> 
				</div>
		  </form>
	</div>
	</div>
  </div>
</div>
</body>
</html>