<!DOCTYPE html>
<html ng-app="search-result" lang="en">
<?php 
include('../config.php');
session_start();

if(!isset ($_SESSION['user'])){
	header ("Location: ../login.php");
}
?>
<head>
	<meta charset=UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>My Collection | <?php echo $_SESSION['user']?></title>
	<meta name="description" content="euTV search interface">
	<meta name="keywords" content="search interface, eutv, music, audio, documents, images, videos">
	<meta name="author" content="Lorenzo Cioni">
	<link rel="image_src" href="../img/micc-logo.png" />
	<link rel="icon" href="../favicon.ico" type="image/x-icon" />
	<link rel="SHORTCUT ICON" href="../favicon.ico" type="image/x-icon" />
	<link rel="stylesheet" href="../css/main.css" type="text/css">
	<link rel="stylesheet" href="../css/fancybox.css" type="text/css">
	<script type="text/javascript" src="../lib/angular.js"></script>
	<script type="text/javascript" src="../lib/jquery-2.0.3.js"></script>
	<script type="text/javascript" src="../lib/jquery-ui-1.10.3.js"></script>
	<script type="text/javascript" src="../lib/jquery.fancybox.js"></script>
	<script type="text/javascript" src="../js/config.js"></script>
	<script type="text/javascript" src="../js/directives.js"></script>
	<script type="text/javascript" src="../js/main.js"></script>
	<script type="text/javascript" src="../js/controllers.js"></script>
</head>
<body ng-controller="mediaCtrl" ng-init="user_init()">
	<div id="toolbar">
		<a href="../index.php">
			<img id="logo" src="../img/micc-logo.png" alt="micc logo">
		</a>
		<ul id="filters">
			<li id="video" ng-click="videoToggle()"></li>
			<li id="image" ng-click="imageToggle()"></li>
			<li id="audio" ng-click="audioToggle()"></li>
			<li id="document" ng-click="documentToggle()"></li>
		</ul>
		<div class="login-menu">
			<?php echo $_SESSION["user"]; ?>
		</div>
		<div id="login-form">
			<p class="email-bold">
				Email: <span class="user-email"><?php echo $_SESSION['email'];?> </span>
			</p>
			<img src="<?php echo $_SESSION['avatar'];?>" alt="avatar">
			<span id="user-panel">
				<a href="../user-panel">User Area</a>
			</span>
			<span id="collection">
				<a href="../user-collection">My Collection</a>
				<form id="form-logout" action="index.php" method="post">
					<br><input id="logout-button" type="button" name="logout" value="Logout">
				</form>
			</span>
		</div>
	</div>
	<div id="wrapper">
		<div id="left-panel">
			<div id="user-area">
				<h3>My Collection</h3>
				<a href="../user-panel">
				<img src="<?php echo $_SESSION['avatar'];?>" alt="avatar">
				</a>
			</div>
			<div id="active-filters">
				<div id="search-panel">
					<form action="../main/index.php" name="search" method="get"
						id="left-search-form">
						<input type="text" name="q" class="input-form-search"
							placeholder="Search" onkeypress="search()" />
						<div id="filedrag">
							Drop here<br>or<br> <input type="file" name="drop_file"
								id="drop_button" value="Select a file" /><br /> <input
								type="submit" name="search-file" value="Search"
								class="search-button">
						</div>
					</form>
					<div id="open-close">&#9654</div>
				</div>
				<h4>
					Active filters
					</h3>
					<ul class="active-filters">
						<li
							ng-repeat="filter in filters | filter:filter.active=true | orderBy:'text'"><div
								class="button-close" ng-click="addRemoveFilter(filter)"></div> <span
							class="active-filter">{{filter.text}}</span></li>
					</ul>
			
			</div>
			<div id="filters-panel">
				<h4>
					Filters
					</h3>
					<input type="text" name="new-filter" id="new-filter"
						ng-model="query" placeholder="Search filter">
					<div id="filters-box">
						<ul class="filter-list">
							<li ng-repeat="filter in filters | filter:query | orderBy:'text'"
								ng-click="addRemoveFilter(filter)">{{filter.text}}
								<div class="check-button" ng-if="filter.active"></div>
							</li>
						</ul>
					</div>
			
			</div>
		</div>
		<div id="results" changeview>
			<div id="search-default">No results found</div>
			<div id="video-panel" class="media-panel" ng-hide="video.active">
				<div class="panel-header">
					<img alt="video" src="../img/video.png">
					<p>Videos</p>
				</div>
				<div id="video-content" class="content">
					<div class="scrollable">
						<img class="loader" src="../img/loader.gif" alt="loader">
						<div
							ng-repeat="cluster in clusters | filter:(cluster.type='video')">
							<div class='cluster' data-title='{{cluster.name.text}}'
								ng-show='isActive("{{cluster.name.text}}")'>
								<img ng-src="{{cluster.media[getRandomIndex(cluster)].thumb}}">
							</div>
							<div ng-repeat="video in cluster.media"
								class='cluster-element video-trigger' href="{{video.src}}"
								data-title="{{video.title}}" data-id="{{video.id}}" cluster="{{cluster.name.text}}">
								<img ng-src="{{video.thumb}}" alt="{{video.tags[0].text}}"
									draggable>
							</div>
						</div>
						<div ng-repeat="video in videos" class="media">
							<a class='video-trigger' href="{{video.src}}" data-id="{{video.id}}"
								data-title="{{video.title}}" ng-show='isVisible(video)'><img
								ng-src="{{video.thumb}}" alt="{{video.tags[0].text}}" draggable>
							</a>
							<div class="star-collection" ng-class="{ 'star-selected' : video.favourite == true}">
								<img src="../img/star.png" ng-class="{ 'star-selected' : video.favourite == true}">
							</div>
						</div>
					</div>
				</div>
			</div>
			<div id="image-panel" class="media-panel" ng-hide="image.active">
				<div class="panel-header">
					<img alt="images" src="../img/image.png">
					<p>Images</p>
				</div>
				<div id="image-content" class="content some-content-related-div">
					<div class="scrollable">
						<img class="loader" src="../img/loader.gif" alt="loader">
						<div
							ng-repeat="cluster in clusters | filter:(cluster.type='image')"
							ng-show='isActive("{{cluster.name.text}}")'>
							<div class='cluster' data-title='{{cluster.name.text}}'>
								<img ng-src="{{cluster.media[getRandomIndex(cluster)].thumb}}">
							</div>
							<div ng-repeat="image in cluster.media"
								class='cluster-element image-trigger'
								data-title="{{image.title}}" data-id="{{image.id}}"
								cluster="{{cluster.name.text}}" href="{{image.src}}">
								<img ng-src="{{image.thumb}}" alt="{{image.tags[0].text}}"
									draggable>
							</div>
						</div>
						<div ng-repeat="image in images" class='media' ng-show='isVisible(image)' ng-hide='!image.favourite' >
							<a class='image-trigger' href="{{image.src}}"
								data-id="{{image.id}}" data-title="{{image.title}}"
								><img ng-src="{{image.thumb}}"
								alt="{{image.tags[0].text}}" draggable> </a>
							<div class="star-collection" ng-class="{ 'star-selected' : image.favourite == true}">
								<img src="../img/star.png" ng-class="{ 'star-selected' : image.favourite == true}">
							</div>
						</div>
					</div>
				</div>
			</div>
			<div id="audio-panel" class="media-panel" ng-hide="audio.active">
				<div class="panel-header">
					<img alt="audio" src="../img/audio.png">
					<p>Audio</p>
				</div>
				<div id="audio-content" class="content">
					<div class="scrollable">
					 <img class="loader" src="../img/loader.gif" alt="loader">
					  <div ng-repeat="audio in audios" class="audio-media" ng-show='isAudioTextVisible(audio)'>
						<a 	class='audio-trigger'
							href="{{audio.src}}" data-id="{{audio.id}}"
							data-title="{{audio.title}}" data-author="{{audio.author}}"
							 tag="{{audio.tags[0].text}}"
							draggable><p class='author'>{{audio.author}}</p>
							<p class='title'>{{audio.title}}</p> </a>
							<div class="star-collection"
								ng-class="{ 'star-selected' : audio.favourite == true}">
								<img src="../img/star.png"
									ng-class="{ 'star-selected' : audio.favourite == true}">
							</div>
					  </div>
					</div>
				</div>
			</div>
			<div id="document-panel" class="media-panel"
				ng-hide="document.active">
				<div class="panel-header">
					<img alt="document" src="../img/document.png">
					<p>Documents</p>
				</div>
				<div id="document-content" class="content">
					<div class="scrollable">
						<img class="loader" src="../img/loader.gif" alt="loader"> 
						<div ng-repeat="document in documents" class="text-media" ng-show='isAudioTextVisible(document)'>
						<a 
							class='document-trigger' href="{{document.src}}"
							data-author="{{document.author}}" data-id="{{document.id}}"
							data-title="{{document.title}}"	
							tag="{{document.tags[0].text}}" draggable><p class='author'>{{document.author}}</p>
							<p class='title'>{{document.title}}</p> </a>
							<div class="star-collection" ng-class="{ 'star-selected' : document.favourite == true}">
								<img src="../img/star.png" ng-class="{ 'star-selected' : document.favourite == true}">
							</div>
							</div>
					</div>
				</div>
			</div>
			<div id="message"></div>
		</div>
	</div>
</body>
</html>
