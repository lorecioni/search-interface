<!DOCTYPE html>
<?php 
include('config.php');
session_start();
if(!isset ($_SESSION['user'])){
	header ("Location: ../login.php");
}
?>
<html lang="en">
<head>
	<title>micc Upload</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="micc search interface">
	<meta name="keywords" content="search interface, micc, music, audio, documents, images, videos">
	<meta name="author" content="Lorenzo Cioni">
	<link rel="image_src" href="img/micc-logo.png" />
	<link rel="icon" href="favicon.ico" type="image/x-icon" />
	<link rel="SHORTCUT ICON" href="../favicon.ico" type="image/x-icon" />
	<link  rel="stylesheet" href="../css/search-template.css" type="text/css">
	<link  rel="stylesheet" href="../css/upload.css" type="text/css">
	<script type="text/javascript" src="../lib/jquery-2.0.3.js"></script>
	<script type="text/javascript" src="../lib/jquery.form.js"></script>
	<script type="text/javascript" src="../js/config.js"></script>
	<script type="text/javascript" src="../js/upload.js"></script>
</head>
<body>
  <div id="container">
	<div id="logo">
		<a href="../">
			<img src="../img/micc-logo.png" alt="micc logo">
		</a>
		<h1>Upload media</h1>
	</div>
	<div id="main-wrapper">	
		<form id="upload-form" action="uploadMedia.php" method="post" enctype="multipart/form-data">
				<div id="filedrag">
    				<div class="dropzone" id="dragandrophandler">
      					<span id="drag-text">Drop file here<br> or</span>					
					</div>
					<div id="progress">
        				<div id="bar"></div>
        				<div id="percent">0%</div>
					</div>
    				<div id="upload-message"></div>
     				<input type="file" size="60" name="myfile" id="query-file" />					
				</div>
				<div id="media-info">
					<input type="text" name="title" class="input" placeholder="Title"/>
					<input type="text" name="author" class="input" placeholder="Author"/>
					<input type="submit" name="search-file" value="Upload" class="search-button"/> 
				</div>
		  </form>
	</div>
	</div>
  </div>
</div>
</html>