<?php
include ('../lib/SmartImage.class.php');
include ('../config.php');
$images = array("gif", "jpeg", "jpg", "png");
$videos = array("mp4", "mpeg", "mov", "avi", "wmv", "mpg");
$documents = array("pdf", "txt", "doc", "docx");
$audios = array("mp3", "wma");
if(isset($_FILES['myfile'])){
	$temp = explode(".", $_FILES["myfile"]["name"]);
	$extension = strtolower(end($temp));
	$media_type = '';
	$upload_dir = '../..'.$uploadDir;
	$id_media_type = 0;
	if(in_array($extension, $images)){
		$upload_dir .= 'image/';
		$media_type = 'image';
		$id_media_type = 2;
	} elseif (in_array($extension, $videos)){
		$upload_dir .= 'video/';
		$media_type = 'video';
		$id_media_type = 1;
	} elseif (in_array($extension, $audios)){
		$upload_dir .= 'audio/';
		$media_type = 'audio';
		$id_media_type = 3;
	} elseif (in_array($extension, $documents)){
		$upload_dir .= 'document/';
		$media_type = 'document';
		$id_media_type = 4;
	} else {
		$upload_dir = null;
	}
	
	if($upload_dir != null ){
		if ($_FILES["myfile"]["error"] > 0) {
			echo "Error: File too large";
		}
		else {
			$filename = str_replace("'", "", str_replace(" ", "", $_FILES["myfile"]["name"]));
			$filename = strtolower(preg_replace('/[^a-zA-Z0-9_ %\[\]\.\(\)%&-]/s', '', $filename));
			move_uploaded_file($_FILES["myfile"]["tmp_name"], $upload_dir.$filename);
			
			//Converting video to mp4
			if($id_media_type == 1){
				$temp_name = explode(".", $filename);
				$out_file = current($temp_name).'.mp4';
				//Generates also video thumbnail
				exec("sh decoder.sh ".$miccDirectory."media/video/".$filename." ".$miccDirectory."media/video/thumb/".$out_file.".jpg 1>encoding-log.txt 2>&1");
				$filename = $out_file;
			} elseif ($id_media_type == 2){
				//Resizing images
// 				$src = $upload_dir.$filename;
// 				$img = new SmartImage($src);
// 				$img->resize(200, 200, true);
// 				$src = '../img/temp/'.$_FILES["file"]["name"];
// 				$img->saveImage($src, 90);
// 				$avatar = $interfacePath.'img/temp/'.$_FILES["file"]["name"];
// 				echo $avatar;
			}
			
			$date = date('Y-m-d H:i:s');
			$dataserverpath = $absolutePath.'media/'.$media_type.'/';
			if(isset($_POST['title'])){
				$title = str_replace("'", "\'", htmlspecialchars($_POST['title']));
			} else {
				$title = '';
			}
			if(isset($_POST['author'])){
				$author = str_replace("'", "\'", htmlspecialchars($_POST['author']));
			} else {
				$author = '';
			}
			$query = mysql_query("INSERT INTO media(id_media_types, uri, dataserverpath, mediauri, title, created, modified, fps, filename, processed_status, id_media_video, last_modified, author) VALUES (".$id_media_type.", '', '".$dataserverpath."', '".$filename."', '".$title."', '".$date."', '".$date."', 0, '".$filename."', 0, NULL, '".$date."', '".$author."' )");
			if(mysql_affected_rows()!= -1){
				echo 'Success';
			} else {
				echo 'Error: mysql error';
			}
		}
	} else {
		echo 'Error: not allowed extension';
	}
} else {
	echo 'Error: file not set';
}
?>