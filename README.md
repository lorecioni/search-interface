![Logo](img/micc-logo-small.png) Search Interface
================

Web application for search in euTV multimedia database.

Framework
---------

- HTML5
- JQuery 2.0.3
- JQuery UI 1.10.3
- AngularJS 1.0.8

Search result are based on search server Apache Solr 4.5.1.

Features
--------

- Searching media in database by keywords or input file
- Similarity search (images only)
- Media preview on mouseover
- Media lightbox on click
- Image and video clustering
- Filtering results
- Start new similarity search by drag and drop
- User media private collection
- Media upload (users with privileges)

The similarity between images is processed by LIRE, Open Source Java Content Based Image Retrieval Library [www.semanticmetadata.net].

In this case I used the Solr plugin of this library, https://bitbucket.org/dermotte/liresolr/overview.
