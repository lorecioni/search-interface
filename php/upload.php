<?php
include ('../lib/SmartImage.class.php');
include ('../config.php');
$allowedExts = array("gif", "jpeg", "jpg", "png");
$temp = explode(".", $_FILES["file"]["name"]);
$extension = end($temp);
if (in_array($extension, $allowedExts))
  {
	if(isset($_FILES['file']) ){
		if ($_FILES["file"]["error"] > 0) {
	 		 echo "Error";
		}
		else {
			$src = $_FILES["file"]["tmp_name"];
    		$img = new SmartImage($src);
    		$img->resize(200, 200, true);
    		$src = '../img/temp/'.$_FILES["file"]["name"];
    		$img->saveImage($src, 90);			
    		$avatar = $interfacePath.'img/temp/'.$_FILES["file"]["name"];
    		echo $avatar;
		}
	}
  }
else
  {
  echo "Error";
  }
?>