$(document).ready(function() {
	window.name = 'null';

	var obj = $("#dragandrophandler");
	obj.on('dragenter', function(e) {
		e.stopPropagation();
		e.preventDefault();
		$(this).parent().css('border-color', 'rgba(255, 255, 255, 0.5)');
		$(this).parent().css('color', 'rgba(255, 255, 255, 0.5)');
	});
	obj.on('dragover', function(e) {
		e.stopPropagation();
		e.preventDefault();
	});
	obj.on('drop', function(e) {
		$(this).parent().css('border-color', 'rgba(255, 255, 255, 0.5)');
		$(this).parent().css('color', 'rgba(255, 255, 255, 0.5)');
		e.preventDefault();
		var files = e.originalEvent.dataTransfer.files;
		console.log(files);
		// We need to send dropped files to Server
		$('#dragandrophandler').fadeOut(0);
		handleFileUpload(files, obj);
	});
	$(document).on('dragenter', function(e) {
		e.stopPropagation();
		e.preventDefault();
	});
	$(document).on('dragover', function(e) {
		e.stopPropagation();
		e.preventDefault();
		$(this).parent().css('border-color', 'rgba(255, 255, 255, 0.5)');
		$(this).parent().css('color', 'rgba(255, 255, 255, 0.5)');
	});
	$(document).on('drop', function(e) {
		e.stopPropagation();
		e.preventDefault();
	});

	var options = {
		beforeSend : function() {
			$("#progress").show();
			$("#bar").width('0%');
			$("#upload-message").html("");
			$("#percent").html("0%");
		},
		uploadProgress : function(event, position, total, percentComplete) {
			$("#bar").width(percentComplete + '%');
			$("#percent").html(percentComplete + '%');

		},
		success : function() {
			$("#bar").width('100%');
			$("#percent").html('100%');

		},
		complete : function(response) {
			window.name = response.responseText;
			$('#upload-message').show();
			$("#upload-message").html("Success!");
			console.log(CFG.absolutePath + "main");
			window.location.replace(CFG.absolutePath + "main");
		},
		error : function() {
			$("#upload-message").html("ERROR!");
			$("#upload-message").addClass('error');
		}
	};
	$("#upload-form").ajaxForm(options);
});

function sendFileToServer(formData,status)
{
    var uploadURL = CFG.absolutePath + "php/upload.php"; //Upload URL
    var extraData = {}; //Extra Data.
    var jqXHR = $.ajax({
            xhr: function() {
            var xhrobj = $.ajaxSettings.xhr();
            if (xhrobj.upload) {
                    xhrobj.upload.addEventListener('progress', function(event) {
                        var percent = 0;
                        var position = event.loaded || event.position;
                        var total = event.total;
                        if (event.lengthComputable) {
                            percent = Math.ceil(position / total * 100);
                        }
                        //Set progress
                        status.setProgress(percent);
                    }, false);
                }
            return xhrobj;
        },
    url: uploadURL,
    type: "POST",
    contentType:false,
    processData: false,
        cache: false,
        data: formData,
        success: function(data){
            status.setProgress(100);
            $('#query-file').prop("disabled",true);
            window.name = data;
         }
    }); 
}

function createStatusbar(obj)
{
     this.statusbar = $("#upload-message");
     this.progressBar = $("#progress");
 
    this.setFileNameSize = function(name,size)
    {
    	$("#upload-message").html(name);
    };
    this.setProgress = function(progress)
    {       
        $('#bar').css('width',progress + '%');
        $('#percent').html(progress + "% ");
    };
}
function handleFileUpload(files,obj)
{
   for (var i = 0; i < files.length; i++) 
   {
        var fd = new FormData();
        fd.append('file', files[i]);        
        var status = new createStatusbar(obj); //Using this we can set progress.
        status.setFileNameSize(files[i].name,files[i].size);
        $("#progress").show();
        $('#upload-message').show();
        sendFileToServer(fd,status);
 
   }
}