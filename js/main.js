$(document).ready(function() {
	
	//Load dropped image
	if(window.name != 'null'){
		var content = '<img class="drop-search-image" src="' + window.name + '">';
		$('.dragged-file').addClass('dropped-file');
		$(".dragged-file").html(content);
	}
	
	$(".video-trigger").fancybox({
		type: 'iframe',
		margin: 0,
		padding: 0,
		maxHeight: 480,
		maxWidth: 800,
		beforeLoad: function() {
            this.title = $(this.element).attr('data-title');
        }
    });
	
	$(".image-trigger").fancybox({
		openEffect	: 'none',
		closeEffect	: 'none',
		padding: 0,
		margin: 0,
		maxHeight: 480,
		beforeLoad: function() {
            this.title = $(this.element).attr('data-title');
        }
	});
	
	$(".audio-trigger").fancybox({
		type: 'iframe',
		margin: 0,
		padding: 0,
		maxHeight: 20,
		maxWidth: 300,
		beforeLoad: function() {
            this.title = $(this.element).attr('data-title');
        }
    });
	
	$(".document-trigger").fancybox({
		type: 'iframe',
		margin: 0,
		padding: 0,
		maxHeight: 480,
		maxWidth: 800,
		iframe: {
            preload: false
        },
		beforeLoad: function() {
            this.title = $(this.element).attr('data-title');
        }
    });
	
});

$(document).on('click', '#open-close', function(e) {
	if ($('#search-panel').css("left") == "-670px") {
		$('#open-close').html('&#9664');
		$('#search-panel').animate({
			left : "+=670px"
		}, 'slow');
		e.stopPropagation();
	} else {
		$('#search-panel').animate({
			left : "-=670px"
		}, 'slow');
		$('#open-close').html('&#9654');
	}
});

//Media reccomend

$(document).on('mouseenter', '.media', function(event) {
	$(this).find('.star').fadeIn('fast');
}).on('mouseleave', '.media', function() {
	$(this).find('.star').fadeOut('fast');
});

$(document).on('mouseenter', '.cluster-media', function(event) {
	$(this).find('.star').fadeIn('fast');
}).on('mouseleave', '.cluster-media', function() {
	$(this).find('.star').fadeOut('fast');
});

$(document).on('mouseenter', '.text-media', function(event) {
	$(this).find('.star').fadeIn('fast');
}).on('mouseleave', '.text-media', function() {
	$(this).find('.star').fadeOut('fast');
});

$(document).on('mouseenter', '.audio-media', function(event) {
	$(this).find('.star').fadeIn('fast');
}).on('mouseleave', '.audio-media', function() {
	$(this).find('.star').fadeOut('fast');
});

// Image tooltip
$(document).on('mouseenter', '.image-media-trigger', function(event) {
	var offset = $(this).parent().position();
	if(offset.left < ($(this).parent().parent().width() - 400)){
		$('#image-content').append('<span class="tooltip right"></span>');
		offset.top += $(this).parent().height()/2 - 10;
		offset.left += ($(this).parent().width()/2 + 5);
	}
	else {
		$('#image-content').append('<span class="tooltip left-image"></span>');
		offset.top += $(this).parent().height()/2 - 10;
		offset.left += ($(this).parent().width()/2 + 5);
	}
	var image_href = $(this).attr("href");
	$('.tooltip').delay(300).fadeIn('slow');
	$('.tooltip').html("<img src='" + image_href + "'>");
	var diff = (230 - $('.tooltip').width());
	if(offset.left > ($(this).parent().parent().width() - 300)){
		offset.left += diff;		
	}
	$('.tooltip').offset(offset);
}).on('mouseleave', '.image-media-trigger', function() {
	$('.tooltip').fadeOut('slow');
	$('.tooltip').remove();
});

//Cluster image tooltip
$(document).on('mouseenter', '.cluster-element', function(event) {
	var offset = $(this).position();
	if(offset.left < ($(this).parent().width() - 400)){
		$('#image-content').append('<span class="tooltip right"></span>');
		offset.top += $(this).height()/2 - 10;
		offset.left += ($(this).width()/2 + 5);
	}
	else {
		$('#image-content').append('<span class="tooltip left-image"></span>');
		offset.top += $(this).height()/2 - 10;
		offset.left += ($(this).width()/2 + 5);
	}
	var image_href = $(this).find('a').attr("href");
	$('.tooltip').delay(300).fadeIn('slow');
	$('.tooltip').html("<img src='" + image_href + "'>");
	var diff = (230 - $('.tooltip').width());
	if(offset.left > ($(this).parent().width() - 300)){
		offset.left += diff;		
	}
	$('.tooltip').offset(offset);
}).on('mouseleave', '.cluster-element', function() {
	$('.tooltip').fadeOut('slow');
	$('.tooltip').remove();
});


// Video tooltip
$(document).on('mouseenter','.video-trigger',function(event) {
			var offset = $(this).parent().position();
			offset.top += $(this).parent().height()/2 - 10;
			offset.left += ($(this).parent().width()/2 + 10);
			$('#video-content').append('<span class="tooltip right"></span>');
			$('.tooltip').offset(offset);
			var video_href = $(this).attr("href");
			$('.tooltip').delay(500).fadeIn('slow');
			$('.tooltip').html("<video autoplay><source src='" + video_href + "' type='video/mp4'></video>");
		}).on('mouseleave', '.video-trigger', function() {
	$('.tooltip').fadeOut('slow');
	$('.tooltip').remove();
});

// Audio tooltip
$(document).on('mouseenter','.audio-trigger', function(event) {
			var offset = $(this).parent().position();
			offset.top += $(this).parent().height()/2 - 10;
			offset.left += ($(this).parent().width()/2);
			$('#audio-content').append('<span class="tooltip left"></span>');
			$('.tooltip').offset(offset);
			var audio_href = $(this).attr("href");
			$('.tooltip').delay(300).fadeIn('slow');

			$('.tooltip').html("<audio autoplay><source src='" + audio_href + "' type='video/mp4'></audio>");

		}).on('mouseleave', '.audio-trigger', function() {
	$('.tooltip').fadeOut('slow');
	$('.tooltip').remove();
});

// Document tooltip
$(document).on('mouseenter','.document-trigger',function(event) {
					var offset = $(this).parent().position();
					offset.top += $(this).parent().height()/2 - 10;
					offset.left += ($(this).parent().width()/2);
					$('#document-content').append('<span class="tooltip left"><div id="tooltip-text"></div></span>');
					$('.tooltip').offset(offset);
					var id = $(this).attr("data-id");
					var url = '../php/solrProxy.php?proxy_url=' + CFG.solrSelectUrl + encodeURIComponent('q=*:*&fq=id:' + id);
					
					$('.tooltip').delay(1000).fadeIn('slow');
					$.ajax({
						url : url,
						dataType : "xml",
						success : function(data) {
							var document = $(data).find('doc');
							
							var text = $(document[0]).find('str[name="text_extracted"]');
							$('#tooltip-text').html(text);
						}
					});
				}).on('mouseleave', '.document-trigger', function() {
			$('.tooltip').fadeOut('slow');
			$('.tooltip').remove();
		});

// Cluster tooltip

$(document).on('mouseenter', '.cluster', function(event) {
	var offset = $(this).position();
	offset.top += $(this).height() / 3 - 30;
	offset.left += $(this).width()* 2 - 15;
	var cluster = $(this).attr('data-cluster-id');
	$(this).parent().parent().append('<span class="cluster-tooltip tooltip"></span>');
	$('.tooltip').offset(offset);
	$('.tooltip').delay(1500).fadeIn('slow');
	var content = '';
	var img = $('.cluster-element[cluster-id="' + cluster + '"]');
	for (var i = 0; $(img[i]).length; i++){
		var src = $(img[i]).find('img').attr('src');
		content = content + '<div class="cluster-preview"><img  src="' + src + '"></div>';
		if(i == 5){
			break;
		}
	}
	$('.tooltip').html(content);
}).on('mouseleave', '.cluster', function() {
	$('.tooltip').fadeOut('slow');
	$('.tooltip').remove();
});

// Open login menù
$(document).on('click', '.login-menu', function(e) {
	$('.login-menu').toggleClass('selected');
	$('#login-form').slideToggle(300);
	e.stopPropagation();
});

// Cluster click

$(document).on('click','.cluster', function() {
					var clusterID = $(this).attr('data-cluster-id');
					if ($('.cluster-element[cluster-id="' + clusterID + '"]').css('display') == 'none') {
						$('.cluster-element[cluster-id="' + clusterID + '"]').fadeIn('slow');
						$(this).find('img').css('border', '4px solid #8E98AD');
						$(this).find('img').css({
											'opacity' : '0.8',
											'-webkit-box-shadow' : '2px 2px 10px 5px rgba(0, 0, 0, 0.4)',
											'-moz-box-shadow' : '2px 2px 10px 5px rgba(0, 0, 0, 0.4)',
											'box-shadow' : '2px 2px 10px 5px rgba(0, 0, 0, 0.4)'
										});
						$(this).addClass('changed');
					} else {
						$('.cluster-element[cluster-id="' + clusterID + '"]').fadeOut('slow');
						$(this).find('img').css('border', '');
						$(this).removeClass('changed');
						$(this).find('img').css(
								{
									'opacity' : '1',
									'-webkit-box-shadow' : '',
									'-moz-box-shadow' : '',
									'box-shadow' : ''
								});
					}
				});

// Close open divs
$(document).click(
		function(e) {
			if (e.target.id != "login-form"
					&& e.target.className != "input-form-login"
					&& e.target.id != "form-login") {
				$('#login-form').slideUp(300);
				$('.login-menu').removeClass("selected");
			}
			if (e.target.id != "search-panel"
					&& e.target.className != "input-form-search"
					&& e.target.id != "filedrag"
					&& e.target.id != "drop_button"
					&& e.target.id != "query-file"
					&& e.target.className != "search-button") {
				$('#search-panel').animate({
					left : "-670px"
				}, 'slow');
				$('#open-close').html('&#9654');
			}
			if (e.target.id == "lightbox" || e.keyCode == 27) {
				$('#content').html('');
				$('#lightbox-panel').css('background-color', 'transparent');
				$("#lightbox, #lightbox-panel, #lightbox-title").fadeOut(300);
			}
		});

$(document).keydown(function(e) {
	if (e.keyCode == 27) {
		$('#content').html('');
		$('#lightbox-panel').css('background-color', 'transparent');
		$("#lightbox, #lightbox-panel, #lightbox-title").fadeOut(500);
		$('#search-panel').animate({
			left : "-670px"
		}, 'slow');
		$('#open-close').html('&#9654');
		$('#login-form').slideUp(300);
		$('.login-menu').removeClass("selected");
	}
});

// Drag&Drop

$(document).on('click', '.dragged-file img', function() {
	window.name = 'null';
	var scope = angular.element(wrapper).scope();
	scope.init();
	scope.$apply();
	$('.dragged-file').removeClass('dropped-file');
	$('.dragged-file').html('Drag file here');
	$(this).remove();
});

$(document).on('mouseenter', '.dragged-file', function(event) {
	if ($(this).hasClass('dropped-file')) {
		$(this).append('<span id="exit-tooltip"></span>');
		$('#exit-tooltip').delay(200).fadeIn('slow');
		$('#exit-tooltip').html("Click to remove");
	}
}).on('mouseleave', '.dragged-file', function() {
	if ($(this).hasClass('dropped-file')) {
		$('#exit-tooltip').fadeOut('slow');
		$('#exit-tooltip').remove();
	}
});

//User collection add/remove

$(document).on('click', '.star', function() {
	var url = CFG.absolutePath + 'user-collection/';
	var media_id = $(this).parent().find('a').attr('data-id');
	var success = '';
	if ($(this).find('img').hasClass('star-selected')) {
		$.ajax({
			url : url + "remove-media.php?media_id=" + media_id,
			method : "GET",
			dataType : "html",
			success : function(data) {
				success = data;
			},
			async: false,
			error : function(err) {
				console.log("Error: " + err);
			}
		});
		if(success == 'true'){
			$(this).find('img').removeClass('star-selected');
			$(this).removeClass('star-selected');
			$('#message').html('Removed from collection!');
			$('#message').fadeIn('slow').delay(1200).fadeOut('slow');
		}
	}
	else{
		$.ajax({
			url : url + "add-media.php?media_id=" + media_id,
			method : "GET",
			dataType : "html",
			success : function(data) {
				success = data;
			},
			async: false,
			error : function(err) {
				console.log("Error: " + err);
			}
		});
		if(success == 'true'){	
			$(this).find('img').addClass('star-selected');
			$(this).addClass('star-selected');
			$('#message').html('Added to collection!');
			$('#message').fadeIn('slow').delay(1200).fadeOut('slow');
		}
		else if(success == 'false'){
			$('#message').html('You must be logged!');
			$('#message').fadeIn('slow').delay(1200).fadeOut('slow');	
		}
	}
});

//User collection add/remove

$(document).on('click', '.star-collection', function() {
	var url = CFG.absolutePath + 'user-collection/';
	var media_id = $(this).parent().find('a').attr('data-id');
	var success = '';
	if ($(this).find('img').hasClass('star-selected')) {
		$.ajax({
			url : url + "remove-media.php?media_id=" + media_id,
			method : "GET",
			dataType : "html",
			success : function(data) {
				success = data;
			},
			async: false,
			error : function(err) {
				console.log("Error: " + err);
			}
		});
		if(success == 'true'){
			angular.element(wrapper).scope().$broadcast('dataloaded');
			$(this).parent().remove();
			$('#message').html('Removed from collection!');
			$('#message').fadeIn('slow').delay(1200).fadeOut('slow');
		}
	}
});

$(document).on('click', '.login-button', function(e){
	e.preventDefault();
	var email = $('#input-form-email').val();
	var password = $('#input-form-password').val();
	    $.ajax({
	        url: '../user-login.php',
	        type: 'POST',
	        data: {
	            email: email,
	            password: password
	        },
	        success: function(msg){
	                console.log(msg);
	            }                   
	        });
	window.location.reload();
});

$(document).on('click', '#logout-button', function(e){
	e.preventDefault();
	    $.ajax({
	        url: '../user-login.php',
	        type: 'POST',
	        data: {
	            logout: true
	        },
	        success: function(msg){
	                console.log(msg);
	            }                   
	        });
	window.location.reload();
});

$(document).on('click', '.search-button', function() {
	if ($('#query-file').val() != '') {
		$('#upload-form').submit();
		$('#dragandrophandler').hide();
	}
	if(window.name != 'null'){
		window.location.reload();
	}
});
