// Media controller , Load and search media

function mediaCtrl($scope, $timeout) {

	//Initialize search view	
	$scope.init = function() {
		// Initialize arrays
		clearScope();
		$scope.favourites = [];
		
		$scope.video = {
			text : 'video',
			active : false
		};
		$scope.image = {
			text : 'image',
			active : false
		};
		$scope.audio = {
			text : 'audio',
			active : false
		};
		$scope.document = {
			text : 'document',
			active : false
		};

		// Search parameter
		if(window.name != 'Error' && window.name != 'null'){
			var url = window.name;
			console.log(url);
			$scope.getSimilarImagesURL(url);
		}
		
		var query = getURLParameter('q');
		if (query == 'null') {
			query = '';
		}

		var query_url = "../php/solrProxy.php?proxy_url=" + CFG.solrSelectUrl + "q=" + encodeURIComponent(query);
		console.log('Query keys: ' + query);
		
		getFavourites();
		
		getVideos(query_url, false);
		getImages(query_url, false);
		getAudios(query_url, false);
		getDocuments(query_url, false);
		
		$timeout(function () { 
			makeClusters();
		}, 1000);
	};
	
	// Initialize my collection	
	$scope.user_init = function() {
		clearScope();
		$scope.video = {
			text : 'video',
			active : false
		};
		$scope.image = {
			text : 'image',
			active : false
		};
		$scope.audio = {
			text : 'audio',
			active : false
		};
		$scope.document = {
			text : 'document',
			active : false
		};

		// Search parameter
		var query = getFavourites();
		var query_url;
		
		query_url = CFG.solrProxyUrl + "?proxy_url=";
		var url = CFG.solrSelectUrl + "q=*&fq=id:" + query;
		query_url += encodeURIComponent(url).replace(/'/g,"%27").replace(/"/g,"%22");
		
		getVideos(query_url, true);
		getImages(query_url, true);
		getAudios(query_url, true);
		getDocuments(query_url, true);
	};
	
	// Shows similar images by id	
	$scope.getSimilarImages = function(id){
		clearScope();
		query = getSimilarImagesID(id);
		query_url = CFG.solrProxyUrl +  "?proxy_url=";
		url = CFG.solrSelectUrl + "q=*&fq=id:" + query;
		query_url += encodeURIComponent(url).replace(/'/g,"%27").replace(/"/g,"%22");
		getImages(query_url, false);
	};
	
	// Shows similar images by URL
	$scope.getSimilarImagesURL = function(url){
		clearScope();
		query = getSimilarImagesURL(url);
		query_url = CFG.solrProxyUrl +  "?proxy_url=";
		url = CFG.solrSelectUrl + "q=*&fq=id:" + query;
		query_url += encodeURIComponent(url).replace(/'/g,"%27").replace(/"/g,"%22");
		getImages(query_url, false);
	};
	
	// Add new filter	
	$scope.addFilter = function(filter) {
		$scope.filters.push(filter);
	};

	// Toggle filters
	$scope.addRemoveFilter = function(filter) {
		if (filter.active == true) {
			filter.active = false;
		} else {
			filter.active = true;
		}
		$scope.$broadcast('dataloaded');
	};

	// Return true if a media element must be hidden or visible
	$scope.isVisible = function(media) {
		if (activeFilters() == 0){
			if (media.inCluster != 0) {
				return false;
			} else {
				return true;
			}
		} else {
				if(media.tags.length == 0){
					return false;
				}
				for ( var i = 0; i < media.tags.length; i++) {
					if (media.tags[i] != undefined && media.tags[i].active == true) {
						return true;
					}
				}
			return false;
		}
	};

	$scope.isAudioTextVisible = function(media) {
		if (activeFilters() == 0) {
			return true;
		} else {
			for ( var i = 0; i < media.tags.length; i++) {
				if (media.tags[i] != undefined && media.tags[i].active == true) {
					return true;
				}
			}
			return false;
		}
	};
	
	// Check if a filter is already in set
	function inSet(filter) {
		var i = 0;
		while (i < $scope.filters.length) {
			if ($scope.filters[i].text == filter) {
				return true;
			}
			i++;
		}
		return false;
	};

	// Return the number of active filters
	function activeFilters() {
		var count = 0;
		for ( var i = 0; i < $scope.filters.length; i++) {
			if ($scope.filters[i].active == true) {
				count++;
			}
		}
		return count;
	};

	//Return the index of a filter in set
	$scope.indexOfFilter = function(filter) {
		var i = 0;
		while (i < $scope.filters.length) {
			if ($scope.filters[i].text == filter) {
				return i;
			}
			i++;
		}
		return null;
	};

	// Change views
	$scope.videoToggle = function() {
		if ($scope.video.active == false) {
			$scope.video.active = true;
			$("#video").css('background-image', 'url("../img/video-h.jpg")');
		} else {
			$scope.video.active = false;
			$("#video").css('background-image', 'url("../img/video.jpg")');
		}
		$scope.$broadcast('dataloaded');
	};

	$scope.imageToggle = function() {
		if ($scope.image.active == false) {
			$scope.image.active = true;
			$("#image").css('background-image', 'url("../img/image-h.jpg")');
		} else {
			$scope.image.active = false;
			$("#image").css('background-image', 'url("../img/image.jpg")');
		}
		$scope.$broadcast('dataloaded');
	};

	$scope.audioToggle = function() {
		if ($scope.audio.active == false) {
			$scope.audio.active = true;
			$("#audio").css('background-image', 'url("../img/audio-h.jpg")');
		} else {
			$scope.audio.active = false;
			$("#audio").css('background-image', 'url("../img/audio.jpg")');
		}
		$scope.$broadcast('dataloaded');
	};

	$scope.documentToggle = function() {
		if ($scope.document.active == false) {
			$scope.document.active = true;
			$("#document").css('background-image',
					'url("../img/document-h.jpg")');
		} else {
			$scope.document.active = false;
			$("#document")
					.css('background-image', 'url("../img/document.jpg")');
		}
		$scope.$broadcast('dataloaded');
	};

	$scope.getRandomIndex = function(cluster) {
		return Math.floor((Math.random() * cluster.media.length));
	};

	//Get videos from database
	function getVideos(url, collection){
		$.ajax({
			 url : url + encodeURIComponent("&fq=type:video"),
			 method : "GET",
			 dataType : "xml",
			 success : function(data) {
				 var videos;
				 videos = $(data).find('doc');
			
				 for ( var k = 0; k < $(videos).length; k++) {
					 var path = $(videos[k]).find('str[name="dataserverpath"]').text();
					 var src = path + $(videos[k]).find('str[name="filename"]').text();
					 var thumb = CFG.mediaDirPath + "video/thumb/" + $(videos[k]).find("str[name='filename']").text() + '.jpg';
					 var title = $(videos[k]).find("str[name='title']").text();
					 var video_tags = $(videos[k]).find('arr').find('str');
					 var id = $(videos[k]).find("str[name='id']").text();
					
					 var tags = [];
				
					 for ( var v = 0; v < $(video_tags).length; v++) {
						 if (!inSet($(video_tags[v]).text())) {
							 $scope.$apply(function() {
								 $scope.filters.push({
									 text : $(video_tags[v]).text(),
									 active : false
								 });
							 });
						 }
						 var index = $scope.indexOfFilter($(video_tags[v]).text());
						 tags.push($scope.filters[index]);		
				     }
						
					 var favourite = false;
						
					if(collection == true){
						favourite = true;
					}
					else {
						//User is connected
						if($scope.favourites != 'false'){		
							if(isFavourite(id, $scope.favourites) == true){
								console.log(id + ' favourite');
								favourite = true;
							}
						}
					}

					$scope.$apply(function() {
						$scope.videos.push({
							id: id,
							src : src,
							thumb : thumb,
							title : title,
							tags : tags,
							favourite: favourite,
							inCluster : false
						});
					});
				}
				$('#video-content .loader').remove();
//				makeClusters();
				$scope.$broadcast('dataloaded');
			},
			error : function(err) {
				console.log("Error: " + err);
			}
		});
	}
	
	//Get images from database	
	function getImages(url, collection){
		$.ajax({
			url : url + encodeURIComponent("&fq=type:image"),
			method : "GET",
			dataType : "xml",
			success : function(data) {
				var images;
				images = $(data).find('doc');

				for ( var i = 0; i < $(images).length; i++) {
					var path = $(images[i]).find("str[name='dataserverpath']")
							.text();

					var src = path
							+ $(images[i]).find("str[name='filename']").text();

					// var thumb = path + 'thumb/'+
					// $(images[i]).find("str[name='mediauri']").text() +
					// 'Thumb.JPG';

					// Temp thumb (missing thumb images)
					var thumb = src;

					var title = $(images[i]).find("str[name='title']").text();
					var id = $(images[i]).find("str[name='id']").text();

					var tags = [];

					var image_tags = $(images[i]).find('arr').find('str');

					for ( var t = 0; t < $(image_tags).length; t++) {
						var temp = ($(image_tags[t]).text().split(/(?:_| )+/));
						for ( var h = 0; h < temp.length; h++) {
							if (temp[h].length > 4) {
								if (!inSet(temp[h])) {
									$scope.$apply(function() {
										$scope.filters.push({
											text : temp[h],
											active : false
										});
									});
								}
							}

							var index = $scope.indexOfFilter(temp[h]);
							tags.push($scope.filters[index]);
						}
					}
					var favourite = false;
					
					if(collection == true){
						favourite = true;
					}
					else {
						//User is connected
						if($scope.favourites != 'false'){		
							if(isFavourite(id, $scope.favourites) == true){
								console.log(id + ' favourite');
								favourite = true;
							}
						}
					}

					$scope.$apply(function() {
						$scope.images.push({
							id : id,
							src : src,
							thumb : thumb,
							title : title,
							tags : tags,
							favourite: favourite,
							inCluster : 0
						});
					});
				}
				
				$('#image-content .loader').remove();
				$scope.$broadcast('dataloaded');
			},
			error : function(err) {
				console.log("Error: " + err);
			}
		});
	}
	
	// Get audio from database	
	function getAudios(url, collection){
		$.ajax({
			url : url + encodeURIComponent("&fq=type:audio"),
			method : "GET",
			dataType : "xml",
			success : function(data) {
				var audios;
				audios = $(data).find('doc');

				for ( var j = 0; j < $(audios).length; j++) {
					var path = $(audios[j]).find("str[name='dataserverpath']").text();
					var src = path + $(audios[j]).find("str[name='filename']").text();
					var title = $(audios[j]).find("str[name='title']").text();
					var author = $(audios[j]).find("str[name='author']").text();
					var id = $(audios[j]).find("str[name='id']").text();
					var audio_tags = $(audios[j]).find('arr').find('str');

					var tags = [];

					for ( var t = 0; t < $(audio_tags).length; t++) {
						var temp = ($(audio_tags[t]).text().split(/(?:_| )+/));
						for ( var h = 0; h < temp.length; h++) {
							if (temp[h].length > 4) {
								if (!inSet(temp[h])) {
									$scope.$apply(function() {
										$scope.filters.push({
											text : temp[h],
											active : false
										});
									});
								}
							}

							var index = $scope.indexOfFilter(temp[h]);
							tags.push($scope.filters[index]);
						}
					}

					var favourite = false;
					
					if(collection == true){
						favourite = true;
					}
					else {
						//User is connected
						if($scope.favourites != 'false'){		
							if(isFavourite(id, $scope.favourites) == true){
								console.log(id + ' favourite');
								favourite = true;
							}
						}
					}
					
					$scope.$apply(function() {
						$scope.audios.push({
							id : id,
							src : src,
							author : author,
							title : title,
							favourite: favourite,
							tags : tags
						});
					});

				}
				$('#audio-content .loader').remove();
				$scope.$broadcast('dataloaded');
			},
			error : function(err) {
				console.log("Error: " + err);
			}
		});
	}
	
	// Get documents from database	
	function getDocuments(url, collection){
		$.ajax({
			url : url + encodeURIComponent("&fq=type:document"),
			method : "GET",
			dataType : "xml",
			success : function(data) {
				var documents;
				documents = $(data).find('doc');

				for ( var s = 0; s < $(documents).length; s++) {
					var path = $(documents[s]).find("str[name='dataserverpath']").text();
					var src = path + $(documents[s]).find("str[name='filename']").text();
					var title = $(documents[s]).find("str[name='title']").text();
					if (title == '') {
						title = $(documents[s]).find("str[name='mediauri']").text();
					}
					var author = $(documents[s]).find("str[name='author']").text();
					if (author == '') {
						author = 'Undefined';
					}
					var id = $(documents[s]).find("str[name='id']").text();

					var tags = [];
//					var doc_tags = $(documents[s]).find('arr').find('str');

//					for ( var t = 0; t < $(doc_tags).length; t++) {
						var temp = ((author + ' ' + title).split(/(?:_| )+/));
						for ( var h = 0; h < 4; h++) {
							if($(temp[h]).length > 2){
								if (!inSet(temp[h])) {
									$scope.$apply(function() {
										$scope.filters.push({
											text : temp[h],
											active : false
										});
									});
								}
							}

							var index = $scope.indexOfFilter(temp[h]);
							tags.push($scope.filters[index]);
						}
						
//					}
						var favourite = false;
						
						if(collection == true){
							favourite = true;
						}
						else {
							//User is connected
							if($scope.favourites != 'false'){		
								if(isFavourite(id, $scope.favourites) == true){
									console.log(id + ' favourite');
									favourite = true;
								}
							}
						}

					$scope.$apply(function() {
						$scope.documents.push({
							id : id,
							src : src,
							author : author,
							title : title,
							favourite: favourite,
							tags : tags
						});
					});
				}
				$('#document-content .loader').remove();
				$scope.$broadcast('dataloaded');
			},
			error : function(err) {
				console.log("Error: " + err);
			}
		});
	}
	
	// Get favourites	
	function getFavourites(){
		//Getting the id of the favourite media	
		var favourites = null;
		$.ajax({
			url : CFG.absolutePath + 'user-collection/favourites.php',
			method : "GET",
			dataType : "html",
			success : function(data) {
				favourites = data;
			},
			async: false,
			error : function(err) {
				console.log("Error: " + err);
			}
		});
		
		var query = '';
		
		if(favourites != 'false' && favourites != null){
			$scope.favourites = favourites.split(',');
			query = '(';
			for (var i = 0; i < $scope.favourites.length - 1; i++){
				console.log($scope.favourites[i]);
				query += $scope.favourites[i] + '%20';
			}
			query += '1)';
		} else {
			query = '0';
		}		
		return query;
	}
	
	// Get similar images id 
	function getSimilarImagesID(id, distance){
		if(typeof(distance)==='undefined') distance = 50;
		
		var query_url = CFG.solrProxyUrl +  "?proxy_url=";
		var images_id = [];
		
		//Getting similar images' id
		var url = CFG.solrLireUrl + "id=" + id;
		query_url += encodeURIComponent(url).replace(/'/g,"%27").replace(/"/g,"%22");
		$.ajax({
			url : query_url,
			method : "GET",
			dataType : "xml",
			success : function(data) {
				var images;
				images = $(data).find('arr').find('lst');
				for ( var i = 0; i < $(images).length; i++) {
					var image_id = $(images[i]).find("str[name='id']").text();
					var d = $(images[i]).find("float[name='d']").text();
					//Take images until a useful distance
					if(d > distance){
						break;
					}
					images_id.push(image_id);
				}
			},
			async: false,
			error : function(err) {
				console.log("Error: " + err);
			}
		});
		
		var query = '(';
		for (var i = 0; i < images_id.length; i++){
			query += images_id[i] + '%20';
		}
		query += '1)';
		
		return query;
	}
	
	// Get similar images id 
	function getSimilarImagesURL(URL, distance){
		if(typeof(distance)==='undefined') distance = 50;
		
		var query_url = CFG.solrProxyUrl +  "?proxy_url=";
		var images_id = [];
		
		//Getting similar images' id
		var url = CFG.solrLireUrl + "url=" + URL;
		query_url += encodeURIComponent(url).replace(/'/g,"%27").replace(/"/g,"%22");
		$.ajax({
			url : query_url,
			method : "GET",
			dataType : "xml",
			success : function(data) {
				var images;
				images = $(data).find('arr').find('lst');
				for ( var i = 0; i < $(images).length; i++) {
					var image_id = $(images[i]).find("str[name='id']").text();
					var d = $(images[i]).find("float[name='d']").text();
					//Take images until a useful distance
					if(d > distance){
						break;
					}
					images_id.push(image_id);
				}
			},
			async: false,
			error : function(err) {
				console.log("Error: " + err);
			}
		});
		
		var query = '(';
		for (var i = 0; i < images_id.length; i++){
			query += images_id[i] + '%20';
		}
		query += '1)';
		
		return query;
	}
	
	// Clear scope
	function clearScope(){	
		$scope.videos = [];
		$scope.images = [];
		$scope.audios = [];
		$scope.documents = [];
		$scope.clusters = [];
		$scope.filters = [];
		console.log('Arrays initialized');
	}
	
	// Get search parameters
	function getURLParameter(name) {
		return decodeURI((RegExp(name + '=' + '(.+?)(&|$)').exec(
				location.search) || [ , null ])[1]);
	}
	
	//Check if a media is in the collection
	function isFavourite(id, favourites){
		for(var i = 0; i < (favourites.length - 1); i++){
			if(id == favourites[i]){
				return true;
			}
		}
		return false;
	}


	// Clusters type
	function cluster(id, type, media) {
		this.id = id;
		this.type = type;
		this.media = media;
	};
	
	function makeClusters() {
		console.log('Clustering images');
		var clusters = [];
		var clusterID = $scope.clusters.length;
		// Make images clusters
		for (var i = 0; i < $scope.images.length; i++) {
			var img = $scope.images[i];
			if(img.inCluster == 0){
				//Find very similar images (distance set to 10)
				var simID = decodeURIComponent(getSimilarImagesID(img.id, 10));
				simID = simID.replace("(", "").replace(" 1)", "").split(" ");
//				console.log(simID);
				if(simID.length > 1){
					var imgs = [];
					clusterID += 1;
					for (var j = 0; j < simID.length; j++) {
						for (var k = 0; k < $scope.images.length; k++){
							if(simID[j] == $scope.images[k].id) {
								imgs.push($scope.images[k]);
								$scope.images[k].inCluster = clusterID;
							}
						}
					}
					if(imgs.length > 1) {
						clusters.push(new cluster(clusterID, 'image', imgs));
					}
				}
			}		
		}
		if(clusters.length > 0){
			$scope.$apply(function() {
				$scope.clusters = clusters;
			});
		}
		console.log(clusters);
	};
};
