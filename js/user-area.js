$(document).ready(function()
{
	var options = { 
    beforeSend: function() 
    {
    	$("#progress").show();
    	$("#bar").width('0%');
    	$("#message").html("");
		$("#percent").html("0%");
    },
    uploadProgress: function(event, position, total, percentComplete) 
    {
    	$("#bar").width(percentComplete+'%');
    	$("#percent").html(percentComplete+'%');

    
    },
    success: function() 
    {
        $("#bar").width('100%');
    	$("#percent").html('100%');

    },
	complete: function(response) 
	{
		$("#message").html("<font color='green'>"+response.responseText+"</font>");
	},
	error: function()
	{
		$("#message").html("<font color='red'> ERROR: impossibile caricare il file. Riprovare.</font>");

	}
     
}; 

     $("#upload-form").ajaxForm(options);

});

$(document).on('click', '.upload-button', function(){
	$('#mod-form').fadeOut(0);
	$('#upload-form').fadeIn('slow');
	$('#progress').fadeIn('slow');
	$('#title').html('Upload avatar');
});