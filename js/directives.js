var search = angular.module('search-result', []);

//search.config([ "$routeProvider", "$locationProvider",
//		function($routeProvider, $locationProvider) {
//			$routeProvider.when("/", {
//				templateUrl : "similarity.html"
//			}).when("/user-collection", {
//				templateUrl : "user-collection.php"
//			}).otherwise({
//				redirectTo : '/'
//			});
//
////			 $locationProvider.html5Mode(true).hashPrefix('!');
//		} ]);

search.directive('draggable', function() {
	return {
		restrict : 'A',
		link : function(scope, elem, attr) {
			$(elem).draggable({
				helper : 'clone',
				appendTo: 'body',
				width: 70,
				height: 70,
				start: function (e, ui) {
			        ui.helper.width(70);
			        ui.helper.height(70);  
			    }
			});
		}
	};
});

search.directive('droppable',function() {
	return {
		restrict : 'A',
		link : function(scope, elem, attr) {
			$(elem).droppable({
				hoverClass : 'dragged-hover',
				tolerance : 'pointer',
				drop : function(event, ui) {
					var src = ui.draggable.attr('src');
					var tags = ui.draggable.attr('alt');
					var id = ui.draggable.attr('data-id');
					var content = '<img src="'+ src + '" alt="'+ tags + '">';
					scope.getSimilarImages(id);
					scope.$apply();
					 $('.dragged-file').addClass('dropped-file');
					 $(".dragged-file").html(content);
				}
			});
		}
	};
});

search.directive('changeview', [ '$timeout', function($timeout) {
	return {
		link : function($scope, element, attrs) {
			$scope.$on('dataloaded', function(event) {
				$timeout(function() {

					if ($scope.video.active == false) {
						$('#video-panel').show();
					}
					if ($scope.image.active == false) {
						$('#image-panel').show();
					}
					if ($scope.audio.active == false) {
						$('#audio-panel').show();
					}
					if ($scope.document.active == false) {
						$('#document-panel').show();
					}

					if ($(".video-trigger:visible").length == 0 && $("#video-content .cluster:visible").length == 0) {
						$('#video-panel').hide();
						resize();
					} else {
						$('#video-panel').show();
						resize();
					}

					if ($(".image-trigger:visible").length == 0 && $("#image-content .cluster:visible").length == 0) {
						$('#image-panel').hide();
						resize();
					} else {
						$('#image-panel').show();
						resize();
					}
					if ($(".audio-media:visible").length == 0) {
						$('#audio-panel').hide();
						resize();
					} else {
						$('#audio-panel').show();
						resize();
					}
					if ($(".text-media:visible").length == 0) {
						$('#document-panel').hide();
						resize();
					} else {
						$('#document-panel').show();
						resize();
					}
					
					$('#message').css('left', $('#results').width()/2 + $('#left-panel').width() - 80);
					$('#message').css('top', $('#results').height()/2);
				}, 0, false);
			});	
		}
	};
} ]);

function resize() {
	var count = numActivePanel();
	$('#search-default').hide();
	
		if (count == 1) {
			$('.media-panel').css("width", "99.8%");
		}
		if (count == 2) {
			$('.media-panel').css("width", "49.82%");
		}
		if (count == 3) {
			$('.media-panel').css("width", "33.15%");
		}
		if (count == 4) {
			$('.media-panel').css("width", "24.8%");
		}
		
		if(count == 0){
			$('#search-default').show();
		}
}

function numActivePanel() {
	var count = 0;
	if ($('#video-panel').css('display') != 'none') {
		count++;
	}
	if ($('#image-panel').css('display') != 'none') {
		count++;
	}
	if ($('#audio-panel').css('display') != 'none') {
		count++;
	}
	if ($('#document-panel').css('display') != 'none') {
		count++;
	}
	return count;
}

$(window).resize(function(){
	clearTimeout($.data(this, 'resizeTimer'));
    $.data(this, 'resizeTimer', setTimeout(function() {
    	resize();
    }, 200));
});