//Global config

var CFG = {
		//Directory absolute path
		absolutePath: 'http://localhost/micc/search-interface/',
		//Media server path
		mediaDirPath: 'http://localhost/micc/media/',
		//Solr path
		solrCoreUrl: 'http://localhost:8080/solr/eutv-media/',
		//Solr select url for search
		solrSelectUrl: 'http://localhost:8080/solr/eutv-media/select?',
		//Solr Lire plugin path
		solrLireUrl: 'http://localhost:8080/solr/eutv-media/lireq?',
		//Solr Proxy url
		solrProxyUrl: 'http://localhost/micc/search-interface/php/solrProxy.php',
};
